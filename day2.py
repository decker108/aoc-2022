score = 0

matrix = { # X/A Rock, B/Y Paper, C/Z Scissors
    'AX': 3, # draw
    'AY': 6, # win
    'BY': 3,
    'BZ': 6,
    'CX': 6,
    'CZ': 3,
}

with open('strategy_guide.txt', 'rt') as fd:
    lines = fd.readlines()

scorerFn = lambda x: 1 if x == 'X' else 2 if x == 'Y' else 3

for line in lines:
    oppMove, yourMove = line.split()
    score += scorerFn(yourMove) + matrix.get(oppMove + yourMove, 0)

print(f'Total score: {score}')
