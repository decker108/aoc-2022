with open('rucksacks2.txt', 'rt') as fd:
    lines = fd.readlines()

score = 0
scoreMap = {chr(v):k for k,v in list(enumerate(range(ord('a'), ord('z')+1), 1)) + list(enumerate(range(ord('A'), ord('Z')+1), 27))}

for line in lines:
    mid = int(len(line)/2)
    firstSet = set([c for c in line[:mid]])
    secondSet = set([c for c in line[mid:]])
    score += scoreMap[firstSet.intersection(secondSet).pop()]

print(f'Total score: {score}')
