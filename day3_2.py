with open('rucksacks2.txt', 'rt') as fd:
    lines = fd.readlines()

score = 0
scoreMap = {chr(v):k for k,v in list(enumerate(range(ord('a'), ord('z')+1), 1)) + list(enumerate(range(ord('A'), ord('Z')+1), 27))}

for idx in range(0, len(lines), 3):
    first, second, third = lines[idx:idx+3]
    firstSet, secondSet, thirdSet = set([*first.strip()]), set([*second.strip()]), set([*third.strip()])
    shared = (firstSet & secondSet & thirdSet).pop() # & is the set intersection operator
    score += scoreMap[shared]

print(f'Total score: {score}')
