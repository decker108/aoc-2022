count = 0
with open('pairs2.txt', 'rt') as fd:
    lines = fd.readlines()

isSubsetFn = lambda left, right: len(left.intersection(right)) > 0
for line in lines:
    left, right = line.split(',')
    leftMin, leftMax, rightMin, rightMax = map(lambda x: int(x), [*left.split('-'), *right.split('-')])
    if isSubsetFn(set(range(leftMin, leftMax+1)), set(range(rightMin, rightMax+1))):
        count += 1

print(f'overlapping pairs: {count}')
