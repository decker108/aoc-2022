elves = {}
idx = 1
with open('calories2.txt', 'rt') as fd:
    for line in fd:
        if line == '\n':
            idx += 1
        elif line != '\n':
            if idx not in elves:
                elves[idx] = 0
            elves[idx] += int(line)

print('max calories', max(elves.values()))
