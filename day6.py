with open('datastream2.txt', 'rt') as fd:
    line = fd.readline()

for i in range(4, len(line)):
    if len(set(line[i-4:i])) == 4:
        print(f'Found start-of-packet marker at index {i} with string {line[i-4:i]}')
        break
