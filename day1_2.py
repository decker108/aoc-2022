elves = {}
idx = 1
with open('calories2.txt', 'rt') as fd:
    lines = fd.readlines()

for line in lines:
    if line == '\n':
        idx += 1
    elif line != '\n':
        if idx not in elves:
            elves[idx] = 0
        elves[idx] += int(line)

top3 = sorted(elves.values())[-3:]
print('top 3 calories', top3)
print('top 3 calories summed', sum(top3))
