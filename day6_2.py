with open('datastream2.txt', 'rt') as fd:
    line = fd.readline()

for i in range(14, len(line)):
    if len(set(line[i-14:i])) == 14:
        print(f'Found start-of-packet marker at index {i} with string {line[i-14:i]}')
        break
