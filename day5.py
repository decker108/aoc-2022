import queue
stackStr, movesStr = '', ''
with open('crates2.txt', 'rt') as fd:
    lines = fd.readlines()
    for line in lines:
        if line == '\n' or line == '':
            pass  # skip empty lines
        if '[' in line:  # stack of crates line
            stackStr += line
        elif 'move' in line:  # crate movement line
            movesStr += line
        elif '1' in line:  # stack numbers line
            stackStr += line

numbersLine = stackStr.strip().split('\n')[-1].strip().split()
stacks = [queue.LifoQueue() for i in range(len(numbersLine))]
crateLines = list(filter(lambda x: len(x) > 0, stackStr.split('\n')[::-1]))[1:]
for crateLine in crateLines:  # split lines, reverse list, skip first row
    j = 0
    for i in range(0, len(numbersLine)*3, 3):  # iterate 3 chars at a time
        slot = crateLine[i + j:i + 3 + j]  # skip 1 char every 3
        if slot != '   ' and len(slot) > 0:
            stacks[j].put(slot[1])
        j += 1

for line in movesStr.strip().splitlines():
    if line == '' or line == '\n':
        continue
    count, origin, dest = int(line.split(' ')[1]), int(line.split(' ')[3]), int(line.split(' ')[5])
    for i in range(count):
        item = stacks[origin-1].get()
        stacks[dest-1].put(item)

idx = 1
for s in stacks:
    print(idx, s.get())
    idx += 1
