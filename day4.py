count = 0
with open('pairs2.txt', 'rt') as fd:
    lines = fd.readlines()

isSubsetFn = lambda l1, l2, r1, r2: (l1 >= r1 and l2 <= r2) or (r1 >= l1 and r2 <= l2) 
for line in lines:
    left, right = line.split(',')
    leftMin, leftMax, rightMin, rightMax = map(lambda x: int(x), [*left.split('-'), *right.split('-')])
    if isSubsetFn(leftMin, leftMax, rightMin, rightMax):
        count += 1

print(f'overlapping pairs: {count}')
