from functools import reduce
LEFT, RIGHT, UP, DOWN = 1, 2, 3, 4
matrix, lines = [], []
with open('trees2.txt', 'rt') as fd:
    for line in fd:
        lines.append(line.replace('\n', ''))
        matrix.append([0 for x in range(len(line.replace('\n', '')))])

def checkTrees(dirx, row, col):
    if dirx == LEFT or dirx == RIGHT:
        horizRange = range(col, -1 if dirx == LEFT else len(matrix[0]), -1 if dirx == LEFT else 1)
        vertRange = [row for _ in horizRange]
    else:
        vertRange = range(row, -1 if dirx == UP else len(matrix[0]), -1 if dirx == UP else 1)
        horizRange = [col for _ in vertRange]
    for row2 in vertRange:
        for col2 in horizRange:
            if not (row == row2 and col == col2) and lines[row2][col2] >= lines[row][col]:
                return False
    return True

for row in range(len(lines)):
    for col in range(len(lines[row])):
        if row == 0 or col == 0 or row == len(lines)-1 or col == len(lines[0])-1:
            matrix[row][col] = 1
        # check if all surrounding trees are lower. if yes, mark visible.
        elif checkTrees(LEFT, row, col) or checkTrees(RIGHT, row, col) or checkTrees(UP, row, col) or checkTrees(DOWN, row, col):
            matrix[row][col] = 1

visibleTrees = 0
for line in matrix:
    visibleTrees += reduce(lambda pv, cv: pv+1 if cv == 1 else pv, line)
print('visibleTrees', visibleTrees)
