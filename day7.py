from dataclasses import dataclass

@dataclass
class File:
    name: str
    size: int = 0

@dataclass
class Folder:
    name: str
    size: int  # TODO remove?
    folders: list
    files: list[File]

inputStr = '''$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k'''

root = Folder(name='/', size=0, folders=[], files=[])
cwd = root
for i in range(len(inputStr.splitlines())):
    line = inputStr.splitlines()[i]
    if line.startswith('$ ls'):
        # TODO iterate thru lines and create files/folders
        pass
    if line.startswith('$ cd'):
        # TODO change cwd
        pass

# TODO recurse thru folders, calc sizes of all folders, sum sizes of all folders <= 100k
