matrix, lines = [], []
with open('trees2.txt', 'rt') as fd:
    for line in fd:
        lines.append(line.replace('\n', ''))
        matrix.append([0 for x in range(len(line.replace('\n', '')))])

def calcVertScore(row, col, _range, score):
    for row2 in _range:
        if lines[row2][col] < lines[row][col]:
            score += 1
        elif lines[row2][col] >= lines[row][col]:
            score += 1
            break
    return score

def calcHorizScore(row, col, _range, score):
    for col2 in _range:
        if lines[row][col2] < lines[row][col]:
            score += 1
        elif lines[row][col2] >= lines[row][col]:
            score += 1
            break
    return score

def calcScenicScore(row, col):
    leftScore = calcHorizScore(row, col, range(col - 1, -1, -1), 0)
    rightScore = calcHorizScore(row, col, range(col + 1, len(matrix[0]), 1), 0)
    upScore = calcVertScore(row, col, range(row - 1, -1, -1), 0)
    downScore = calcVertScore(row, col, range(row + 1, len(matrix[0]), 1), 0)
    return leftScore * rightScore * upScore * downScore

for row in range(len(lines)):
    for col in range(len(lines[row])):
        matrix[row][col] = calcScenicScore(row, col)

hiScore = 0
for line in matrix:
    # print(line)
    hiScore = max(hiScore, max(line))
print(f'Highest scenic score: {hiScore}')
